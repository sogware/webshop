<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class CartController extends Controller
{

    /**
     * @Route("/winkelwagen", name="winkelwagen")
     */
    public function indexAction(Request $request)
    {

        // get contents of cart
        $session = $request->getSession();
        $contents = $session->get('cartContents');
        if (!isset($contents)) {
            return $this->render('cart/empty.html.twig');
        }

        // get the product information in cart
        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $products = $repository->findBy(array('id' => array_keys($contents)));

        $cart = [
            'products' => []
        ];
        $totalQuantity = 0;
        $totalAmount = 0;
        $vatBuckets = [];

        foreach ($products as $product) {

            // get values
            $vatPercentage = $product->getVat();
            $qty = $contents[$product->getId()];
            $price = $product->getPrice();

            // do some calculations
            $priceExVat = $price / (1 + $vatPercentage / 100); // eg 10 / 1.06
            $vatAmount = ($price - $priceExVat) * $qty;
            $subExVat = $priceExVat * $qty;
            $subIncVat = $price * $qty;
            $totalQuantity += $qty;
            $totalAmount += $subIncVat;

            // fill cart
            $cart['products'][$product->getId()] = array(
                'name' => $product->getName(),
                'image' => $product->getImage(),
                'qty' => $qty,
                'priceIncVat' => $price,
                'vatPercentage' => $vatPercentage,
                'priceExVat' => $priceExVat,
                'subExVat' => $subExVat,
                'subIncVat' => $subIncVat,
            );

            // fill the vatBuckets
            if ( !isset($vatBuckets[$vatPercentage]) ) {
                $vatBuckets[$vatPercentage] = array(
                    'subExVat' => 0,
                    'vatAmount' => 0
                );
            }
            $vatBuckets[$vatPercentage]['subExVat'] += $subExVat;
            $vatBuckets[$vatPercentage]['vatAmount'] += $vatAmount;

        }

        // add totals
        $cart['total'] = array(
            'qty' => $totalQuantity,
            'amount' => $totalAmount,
            'vat' => $vatBuckets
        );

        return $this->render('cart/list.html.twig', array('cart' => $cart));

    }

    /**
     * @Route("/winkelwagen/voegtoe/{productId}", name="inwinkelwagen")
     */
    public function addAction(Request $request, $productId)
    {
        $session = $request->getSession();

        $contents = $session->get('cartContents');
        if (isset($contents[$productId])) {
            $contents[$productId] += 1;
        } else {
            $contents[$productId] = 1;
        }
        if (isset($contents['total'])) {
            $contents['total'] += 1;
        } else {
            $contents['total'] = 1;
        }
        $session->set('cartContents', $contents);
        $session->set('cartQty', $contents['total']);

        return $this->redirectToRoute('winkelwagen');

    }

    /**
     * @Route("/winkelwagen/kiepom", name="leegwinkelwagen")
     */
    public function clearAction(Request $request)
    {

        $session = $request->getSession();
        $session->remove('cartContents');
        $session->set('cartQty', '');
        $session->getFlashBag()->add('notice', "Winkelwagen is leeggemaakt");
        return $this->redirectToRoute('homepage');

    }

}
